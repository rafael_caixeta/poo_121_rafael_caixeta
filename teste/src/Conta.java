
public class Conta {
	
	static int contador = 1000; 
	
	
	//Declara��o de vari�veis
	private int numero;
	private double saldo;
	private boolean ativa;
	
	
	//Construtor
	public Conta(double saldo) {
		
		this.numero = contador++;
		this.saldo = saldo;
		
	}
	
	public Conta(double saldo, boolean status) {
		
		this.numero = contador++;
		this.saldo = saldo;
		this.ativa = true;
		
	}
	
	//M�todo de acesso p�blico
	
	
	public int getNumero() {
		return numero;
	}
	
	public void setNumero(int numero) {
		this.numero = numero;
	}
	
	public double getSaldo() {
		return saldo;
	}
	
	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}
	
	//metodos de neg�cio
	public void sacar(double numero) {
		this.saldo -= numero;
	}
	
	public void depositar(double numero) {
		this.saldo += numero;
	}
	
}
